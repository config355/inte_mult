using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[System.Serializable]
public class Platform
{ 
    public MovingPlatform movingPlatform; 
}
public class PlatformSwitch : MonoBehaviour, IInteractable
{
    [SerializeField] private string _prompt;
    [SerializeField] private GameObject _canvas;
    [SerializeField] private bool _enable = true;
    [SerializeField] private bool _restart = false;
    public string prompt => _prompt;
    public GameObject canvas => _canvas;

    public bool enable  => _enable;

    public bool restart => _restart;

    public float speed = 1.0f;
    public Platform[] platforms;
    private int platformMoving = 0;


    public bool Interact(Interactor interactor)
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            
            foreach (Platform item in platforms)
            {
                platformMoving++;

                item.movingPlatform.Move(this) ;
                this._enable = false;
            }
            
        }
        return true;
    }

    public void movingFinihed()
    {
        platformMoving--;
        if (platformMoving == 0 && restart)
        {
            this._enable = true;
        }
    }



}
