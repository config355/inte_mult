using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
 
    [SerializeField]
    private float _speed;
    [SerializeField]
    private bool loop;

    [SerializeField]
    private Transform endTransform;

    private PlatformSwitch platformSwitch ;

    private bool move;
    private float startTime;
    private Vector3 startPosition;
    private Vector3 endPosition;

    private void Start()
    {
        startTime = Time.time;
        startPosition = this.transform.position;
        endPosition = endTransform.position;
        move = false;
    }

    private void FixedUpdate()
    {
        if (move)
        {
            float journeyLength = Vector3.Distance(startPosition, endPosition);

            float distCovered = (Time.time - startTime) * _speed;
            float fractionJourney = distCovered / journeyLength;

            if (fractionJourney < 1)
            {
                transform.position = Vector3.Lerp(startPosition, endPosition, fractionJourney);
            }
            else 
            {
                startTime = Time.time;

                Vector3 temp = startPosition;
                startPosition = endPosition;
                endPosition = temp;

                if ( loop == false )
                {
                    platformSwitch.movingFinihed();
                }

                move = loop;
            }
        }

    }

    public void Move(PlatformSwitch platformSwitch)
    {
        this.platformSwitch = platformSwitch;
        startTime = Time.time;
        this.move = true;
    }

    public bool isMoving()
    {
        return move;
    }

    private void OnTriggerEnter(Collider other)
    {
        other.transform.SetParent(transform);
    }

    private void OnTriggerExit(Collider other)
    {
        other.transform.SetParent(null);
    }

   private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(this.transform.position, endTransform.position); 

    }
  
}
