using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private CharacterController controller;
    [SerializeField]
    private Vector3 playerVelocity;
    [SerializeField]
    private bool groundedPlayer;
    private float playerSpeed = 2.0f;
    [SerializeField]
    private float jumpHeight = 1.0f;
    private float gravityValue = -9.81f;

    [SerializeField]
    bool isJumping;
    [SerializeField]
    private float lowMultiplier = 1.5f;
    [SerializeField]
    private float highMultiplier = 2.0f;
    [SerializeField]
    bool isPressingJump;
    [SerializeField]
    bool hasReleasedJump;

    [SerializeField]
    float? lastGroundedTime;
    [SerializeField]
    float graceTime = 0.5f;
    [SerializeField]
    float? lastJumpPressedTime;


    [SerializeField]
    private Transform cameraTransform;

    private void Start()
    {
        controller = gameObject.AddComponent<CharacterController>();
        isJumping = !controller.isGrounded;
        isPressingJump = Input.GetButton("Jump");

    }

    void Update()
    {
        groundedPlayer = controller.isGrounded;
        if (groundedPlayer) { lastGroundedTime = Time.time; }

        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        // move where the camera is facing
        move = (Quaternion.AngleAxis(cameraTransform.rotation.eulerAngles.y, Vector3.up) * move);
        controller.Move(move * Time.deltaTime * playerSpeed);

        if (move != Vector3.zero)
        {
            Quaternion toRotation = Quaternion.LookRotation(move, Vector3.up);

            float maxDegreesDelta = 180.0f * Time.deltaTime;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, maxDegreesDelta);

        }

        if (Input.GetButtonDown("Jump"))
        {
            lastJumpPressedTime = Time.time;
        }


        playerVelocity.y += MultiplyGravity(playerVelocity) * Time.deltaTime;
        if (groundedPlayer && playerVelocity.y < 0)
        {
            playerVelocity.y = -0.5f;
            isJumping = false;
        }

        // Changes the height position of the player..
        if ((Time.time - lastGroundedTime) < graceTime & (Time.time - lastJumpPressedTime) < graceTime)
        {
            playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            isJumping = true;
            hasReleasedJump = false;
            lastGroundedTime = null;
            lastJumpPressedTime = null;
        }

        if (isJumping)
        {
            isPressingJump = Input.GetButton("Jump");
        }



        controller.Move(playerVelocity * Time.deltaTime);
    }

    private float MultiplyGravity(Vector3 playerVelocity)
    {
        if (isJumping & playerVelocity.y > 0 & (isPressingJump == false || hasReleasedJump == true))
        {
            hasReleasedJump = true;
            return gravityValue * lowMultiplier;
        }

        if (isJumping & playerVelocity.y < 0)
        {
            return gravityValue * highMultiplier;
        }

        return gravityValue;
    }
}
