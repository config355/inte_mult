# Instalar #

copiar las carpetas `Models`, `Animations`, `Animators` y el script `Movement.cs`
en el proyecto unity.



Modificar el modelo `Y Bot`
	
	Rig.Animation Type = Humanoide
	Rig.Avatar Definition = Create From This Model
	Apply

Modificar cada animación

	Rig.Animation Type = Humanoide
	Rig.Avatar Definition = Copy From Other Avatar
	Rig.Source = Y BotAvatar
	Apply


En la animación `Idle`, `Jumpin Up`, `Falling Idle`, `Falling to Landing` 

	activar `Loop Time`
	activar `Loop Pose`
	En todo los [Root Transform]
	Bake into Pose
	Base Upon Original



En la animación Walk 
	
	activar `Loop Time`
	activar `Loop Pose`
	[Root Transform Rotation, Root Transform Position(y)]
	Bake into Pose
	Base Upon Original


En `Animators.YBot.controller` agregar las animaciones a cada estado 

	Idle.motion = idle
	`JumpinUp.motion`.motion = JumpingUp
	`Falling Idle`.motion = `Falling Idle`
	`Falling To Landing`.motion = `Falling To Landing`


![motion](motion.png)


Agregar `Y Bot` al escenario.
Agregar el componente `Character controller` y ajustar el collider.
Agregar el script `Movement.cs`
Agregar el componente `Animator`
	
	controller = `Y Bot.controller`
	avatar = `Y BotAvatar`
	Apply Root Motion = Y



